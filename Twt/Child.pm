package Twt::Child;
use Moose::Role;

has parent => (
    is       => 'ro',
    isa      => 'Twt',
    required => 1,
    weak_ref => 1,
);

no Moose;
1
