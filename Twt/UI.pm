package Twt::UI;
use Moose::Role;
use Twt::Child;

with 'Twt::Child';
requires qw(append show_tweet show_error run);

no Moose;
1
