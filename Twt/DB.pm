package Twt::DB;
use Moose;
use DBM::Deep;
use List::Util            qw(any);
use File::HomeDir         qw(my_data);
use File::Spec::Functions qw(catfile catdir);
use Twt::Child;


with 'Twt::Child';

has dbm => (
    is      => 'rw',
    isa     => 'DBM::Deep',
    lazy    => 1,
    default => sub
    {
        my $user = shift->parent->user->{screen_name};

        my $datadir = catdir(my_data, '.twt');
        if (!-d $datadir)
        {   mkdir $datadir or die "Can't make data directory '$datadir': $!\n" }

        DBM::Deep->new(catfile($datadir, ".twt.$user.db"))
    },
);

has selected => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {} },
);


sub last_tweet
{
    my ($self, $tweet_id) = @_;
    if (defined $tweet_id)
    {   $self->dbm->{last_tweet} = $tweet_id }
    else
    {   $self->dbm->{last_tweet} }
}


sub last_mention
{
    my ($self, $user, $tweet_id) = @_;
    if (defined $tweet_id)
    {   $self->dbm->{last_mention}{$user} = $tweet_id }
    else
    {   $self->dbm->{last_mention}{$user} }
}


sub select
{
    my ($self, $user, $tweet_id) = @_;
    if (@_ < 2)
    {   %{$self->selected} = () }
    elsif (@_ < 3)
    {   $self->selected->{$user} }
    else
    {   $self->selected->{$user} = $tweet_id }
}


sub tweets_from
{
    my ($self, $screen_name) = @_;
    $self->dbm->{tweets}{$screen_name}
}


sub on_tweet
{
    my ($self, $tweet) = @_;
    my  $screen_name   = $tweet->{user}{screen_name};

    $self->last_tweet($tweet->{id});
    $self->dbm->{tweets}{$screen_name}{$tweet->{id}} = $tweet->{text};

    my $mentions = $tweet->{entities}{user_mentions} || [];
    my $uid      = $self->parent->user->{id};
    if (any { $_->{id} == $uid } @$mentions)
    {
        $self->last_mention($screen_name, $tweet->{id});
    }
}

sub tweet
{
    my ($self, $tweet_id) = @_;
    while (my ($user, $tweets) = each %{$self->dbm->{tweets}})
    {
        return $user, $tweets->{$tweet_id} if exists $tweets->{$tweet_id};
    }
    ()
}


sub remember_color
{
    my $self = shift;
    $self->dbm->{color}{$_->{screen_name}} = $_->{profile_link_color} for @_;
}

sub color
{
    my ($self, $name) = @_;
    if ($name eq $self->parent->user->{screen_name} && $self->parent->my_color)
    {   $self->parent->my_color }
    else
    {   $self->dbm->{color}{$name} }
}


no Moose;
1
