package Twt::Stream;
use Moose;
use AnyEvent::Twitter::Stream;
use List::Util qw(min max);
use Twt::Child;


with 'Twt::Child';

has stream => (
    is => 'rw',
);

has delay  => (
    is      => 'rw',
    isa     => 'Int',
    default => 0,
);


sub BUILD
{
    my ($self) = @_;

    $self->stream(AE::timer $self->delay, 0, sub
    {
        $self->stream(AnyEvent::Twitter::Stream->new(
            $self->parent->secrets,
            $self->parent->events,
            method     => 'userstream',
            timeout    => 90,
        ));
    });
}


sub next_delay
{   min(320, max(shift->delay * 2, 5)) }


no Moose;
1
