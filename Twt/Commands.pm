package Twt::Commands;
use Moose;
use List::Util qw(any);
use Twt::Child;
use Twt::Command;


our @BUILTINS;
sub builtin
{
    push @BUILTINS, Twt::Command->new(
        names      => shift,
        short_help => shift,
        long_help  => shift,
        callback   => shift,
    );
}


with 'Twt::Child';

has commands => (
    is      => 'ro',
    isa     => 'ArrayRef[Twt::Command]',
    default => sub { [@BUILTINS] },
);


sub find
{
    my ($self, $cmd) = @_;
    grep { any { $_ eq $cmd } @{$_->names} } @{$self->commands};
}


sub execute
{
    my ($self, $line) = @_;

    return unless $line =~ /\S/;
    $line =~ s/^\s+|\s+$//g;

    my ($cmd, $arg) = split ' ', $line, 2;

    my @got = $self->find(lc $cmd);
    die "No such command: '$cmd'\n"      if @got < 1;
    die "Command '$cmd' is ambiguous.\n" if @got > 1;
    $got[0]->run($self->parent, $cmd, $arg // '');
}



builtin ['tweet', 't'], 'tweet TEXT: make a basic tweet', <<'{', sub {
Usage: tweet TEXT
Make a basic tweet, it won't be a reply or a retweet.
You can attach a file to your tweet by putting [[/a/path/to/media.file]]
into your TEXT.
{
    my (undef, $twt, $cmd, $status) = @_;
    die "Usage: $cmd TWEET TEXT\n" unless length $status;
    $twt->rest->tweet({status => $status});
};


builtin ['reply', 'r'], 'reply @USER TEXT: tweet reply to USER', <<'{', sub {
Usage: reply @USER TEXT
Sends a tweet as a reply to the selected tweet from USER (see 'list'
command) or, in absense of that, the last tweet that @mentioned you.
You can attach a file to your tweet by putting [[/a/path/to/media.file]]
into your TEXT.
{
    my (undef, $twt, $cmd, $status) = @_;
    die "Usage: $cmd \@MENTION TWEET\n" unless $status =~ /^\@\w+\s+\S/;

    my ($user) = $status =~ /^\@(\w+)/
        or die "Reply requires \@mention at start of reply.\n";
    my $reply  = $twt->db->select($user) || $twt->db->last_mention($user)
        or die "Nothing from '$user' to reply to.\n";

    $twt->rest->tweet({
        status                => $status,
        in_reply_to_status_id => $reply,
    });

    if ($twt->db->select($user))
    {
        $twt->db->select($user, undef);
        $twt->ui->append("Cleared selected tweet for \@$user.");
    }
};


builtin ['delete', 'd'], 'delete [DELTA]', <<'{', sub {
Usage: delete [DELTA]
Delete the last DELTA tweet, defaulting to 1. So `delete` or `delete 1`
would delete the last tweet made, while `delete 2` would delete the
second-last tweet et cetera.
{
    my (undef, $twt, undef, $delta) = @_;
    $delta = 1 if not length $delta;

    die "Tweet delete delta must be a positive integer.\n"
        if $delta =~ /\D/ || $delta < 1;

    my $tweet = $twt->rest->last_own_tweet($delta)
        or die "Couldn't find tweet at delta '$delta'. "
             . "Either it's too old or it doesn't exist.";

    if ($twt->rest->untweet($tweet->{id}))
    {
        $twt->ui->append("Deleted tweet: '$tweet->{text}'");
    }
    else
    {
        die "Can't delete tweet for some reason: '$tweet->{text}'\n";
    }
};


builtin ['follow', 'f'], 'follow [@]USER: start following USER', <<'{', sub {
Usage: follow [@]USER
You'll start following the given USER.
{
    my (undef, $twt, $cmd, $user) = @_;
    die "Usage: $cmd [\@]USER\n" unless $user =~ /^\@?(\w+)$/;
    $twt->rest->follow($user);
};


builtin ['select', 's'], 'select [@]USER [REGEX]: pick a tweet to reply to',
        <<'{', sub {
Usage: select [[@]USER [REGEX]]
Show tweets of the given USER [matching REGEX].
The latest tweet shown will be selected and will be replied to when you
use the 'reply' command.
If no USER is given, deselects all tweets.
{
    my (undef, $twt, $cmd, $arg) = @_;
    my ($user, $pattern)         = split ' ', $arg, 2;

    if (length $user)
    {
        $user      =~ s/^\@//;
        my $regex  = length $pattern ? qr/$pattern/ : undef;
        my $tweets = $twt->db->tweets_from($user);
        die "No tweets from \@$user.\n" if not $tweets;

        my $last;
        for (sort keys %$tweets)
        {
            my $text = "$_ $tweets->{$_}";
            if (!$regex || $text =~ $regex)
            {
                $twt->ui->show_tweet($user, $text);
                $last = $_;
            }
        }

        $twt->db->select($user, $last);
        $twt->ui->append($last ? "Selected tweet $last for \@$user."
                               : "Cleared selected tweet for \@$user.");
    }
    else
    {
        $twt->db->select;
        $twt->ui->append('Cleared all selected tweets.');
    }
};


builtin ['help', 'h'], 'help [COMMAND]: show help (for COMMAND)', <<'{', sub {
Usage: help
Show all commands and their short help messages.
Usage: help COMMAND
Show long help message about the given COMMAND.
{
    my (undef, $twt, $cmd, $about) = @_;
    my $commands = $twt->commands;
    my $ui       = $twt->ui;

    if (length $about)
    {
        my @got = $commands->find(lc $about);
        $ui->show_error("No such command: '$about'\n")      if @got < 1;
        $ui->show_error("Command '$about' is ambiguous.\n") if @got > 1;
        $ui->append($_->long_help) for @got;
    }
    else
    {
        $ui->append('Available commands:');
        for (@{$commands->commands})
        {
            my $names = join ', ', @{$_->names};
            $ui->append("\t$names - " . $_->short_help);
        }
    }
};


no Moose;
1
