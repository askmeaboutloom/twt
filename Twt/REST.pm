package Twt::REST;
use Moose;
use File::HomeDir qw(my_home);
use Net::Twitter;
use Twt::Child;


with 'Twt::Child';

has twitter => (
    is  => 'rw',
    isa => 'Net::Twitter',
);


sub BUILD
{
    my ($self) = @_;

    my %secrets = $self->parent->secrets;
    $secrets{access_token       } = delete $secrets{token       };
    $secrets{access_token_secret} = delete $secrets{token_secret};

    $self->twitter(Net::Twitter->new(
        %secrets,
        traits => ['API::RESTv1_1'],
    ));
}


sub get_account
{
    my ($self) = @_;
    $self->twitter->verify_credentials
}


sub tweets_since_last_time
{
    my ($self) = @_;
    my $last_tweet = $self->parent->db->last_tweet;
    my %since      = defined $last_tweet ? (since_id => $last_tweet) : ();
    reverse @{$self->twitter->home_timeline({count => 200, %since})}
}

sub last_own_tweet
{
    my ($self, $delta) = @_;
    $self->twitter->user_timeline({count => $delta})->[$delta - 1]
}


sub tweet
{
    my ($self, $tweet) = @_;
    my $method = 'update';

    while ($tweet->{status} =~ /\[\[(.+?)\]\]/g)
    {
        (my $path = $1) =~ s{^~(?=/)}{my_home}e;
        die "Not a file: $path\n" if not -f $path;
        push @{$tweet->{'media[]'}}, $path;
        $method = 'update_with_media';
    }
    $tweet->{status} =~ s/\[\[.+?\]\]//g;

    $self->twitter->$method($tweet)
}

sub untweet
{
    my ($self, $id) = @_;
    $self->twitter->destroy_status($id)
}


sub follow
{
    my ($self, $name) = @_;
    $self->twitter->follow({screen_name => $name});
}


no Moose;
1
