package Twt::Command;
use Moose;


has names => (
    is       => 'rw',
    isa      => 'ArrayRef[Str]',
    required => 1,
);

has short_help => (
    is      => 'rw',
    isa     => 'Str',
    default => 'No short help registered.',
);

has long_help => (
    is      => 'rw',
    isa     => 'Str',
    default => 'No long help registered.',
);

has callback => (
    is       => 'rw',
    isa      => 'CodeRef',
    required => 1,
);


sub run { $_[0]->callback->(@_) }


no Moose;
1
