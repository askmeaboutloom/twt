package Twt::UI::Term;
use Moose;
use AnyEvent;
use Encode            qw(encode decode);
use String::Escape    qw(unbackslash);
use Twt::UI;


with 'Twt::UI';

has cv => (
    is      => 'ro',
    default => sub { AE::cv },
);

has want_color => (
    is      => 'ro',
    default => sub
    {
        -t STDOUT and eval
        {
            require Color::ANSI::Util;
            require Term::ANSIColor;
            *ansifg  = \&Color::ANSI::Util::ansifg;
            *color   = \&Term::ANSIColor::color;
            *colored = \&Term::ANSIColor::colored;
            1
        };
    },
);

has io => (
    is => 'rw',
);

has print => (
    is  => 'rw',
    isa => 'CodeRef',
);


sub BUILD
{
    my ($self) = @_;

    my $user = $self->parent->user;
    $self->parent->db->remember_color($user);

    if (-t STDIN && -t STDOUT && eval { require AnyEvent::ReadLine::Gnu })
    {
        my $prompt = "$user->{screen_name} > ";

        if ($self->want_color)
        {
            my $color  = ansifg($self->parent->my_color
                             // $user->{profile_link_color}
                             // 'ffffff');
            my $reset  = color('reset');
            $prompt = "\001$color\002$prompt\001$reset\002";
        }

        $self->io(AnyEvent::ReadLine::Gnu->new(
            prompt  => $prompt,
            on_line => sub { $self->on_line(@_) },
        ));
        $self->print(sub { $self->io->print(@_) });
    }
    else
    {
        $self->io(AnyEvent->io(
            fh => \*STDIN,
            poll => 'r',
            cb   => sub { $self->on_line(scalar <STDIN>) },
        ));
        $self->print(sub { print @_ });
    }
}


sub append
{
    my $self  = shift;
    my @lines = map { /\n$/ ? $_ : "$_\n" } @_;
    my $text  = "@lines";

    if ($self->want_color)
    {
        my %users;
        while ($text =~ /\@(\w+)/g)
        {   $users{$1} = $self->parent->db->color($1) // 'ffffff' }

        while (my ($user, $color) = each %users)
        {
            my $colored = ansifg($color) . "\@$user" . color('reset');
            $text =~ s/\@\Q$user\E/$colored/g;
        }
    }

    $self->print->(encode 'UTF-8', $text, Encode::FB_WARN);
}


sub show_tweet
{
    my ($self, $user, $text) = @_;
    $self->parent->db->remember_color($user) if ref $user;

    my $name   = ref $user ? "\@$user->{screen_name}:" : "\@$user:";
    my $indent = ' ' x length $name;

    my ($first, @lines) = split "\n", $text;
    $self->append("$name $first", map { "$indent$_" } @lines);
}


sub show_event
{
    my ($self, $event) = @_;
    $self->parent->db->remember_color(@{$event}{'source', 'target'});
    $self->append("\u$event->{event} "
           . "from \@$event->{source}{screen_name} "
           .   "to \@$event->{target}{screen_name}");
}


sub show_error
{
    my $self  = shift;
    my $error = "ERROR: @_";
    $error =~ s/\s+$//;
    $self->append($self->want_color ? colored($error, 'red') : $error);
}


sub on_line
{
    my ($self, $line) = @_;
    return $self->cv->send if not defined $line;
    eval
    {
        my $decoded = unbackslash(decode 'UTF-8', $line, Encode::FB_CROAK);
        $self->parent->send($decoded)
    };
    $self->show_error($@) if $@;
}


sub run
{
    my ($self) = @_;
    $self->cv->recv;
}


no Moose;
1
