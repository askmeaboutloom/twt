package Twt;
use Moose;
use MooseX::StrictConstructor;
use Data::Dumper;
use HTML::Entities qw(decode_entities);
use Twt::Commands;
use Twt::DB;
use Twt::REST;
use Twt::Stream;
use Twt::UI;
use constant EMPTY_SUB => sub {};


has consumer_key => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has consumer_secret => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has token => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has token_secret => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has user => (
    is      => 'rw',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub { shift->rest->get_account },
);

has my_color => (
    is  => 'rw',
    isa => 'Str',
);

has commands => (
    is      => 'rw',
    isa     => 'Twt::Commands',
    lazy    => 1,
    default => sub { Twt::Commands->new(parent => shift) },
);

has db => (
    is      => 'rw',
    isa     => 'Twt::DB',
    lazy    => 1,
    default => sub { Twt::DB->new(parent => shift) },
);

has ui => (
    is      => 'rw',
    isa     => 'Twt::UI',
    lazy    => 1,
    default => sub
    {
        require Twt::UI::Term;
        Twt::UI::Term->new(parent => shift)
    },
);

has stream => (
    is      => 'rw',
    isa     => 'Twt::Stream',
    clearer => 'clear_stream',
    lazy    => 1,
    default => sub { Twt::Stream->new(parent => shift) },
);

has rest => (
    is      => 'rw',
    isa     => 'Twt::REST',
    lazy    => 1,
    clearer => 'clear_rest',
    default => sub { Twt::REST->new(parent => shift) },
);


for (__PACKAGE__->meta->get_all_methods)
{
    my $method = $_->name;
    my $plug   = "${method}_plug";
    $method =~ /^on_/ or next;

    has $plug => (
        is      => 'rw',
        isa     => 'CodeRef',
        default => \&EMPTY_SUB,
    );

    before $method => sub
    {
        my ($self) = @_;
        eval { $self->$plug->(@_) };
        $self->ui->error($@) if $@;
    };
}


sub secrets
{
    my ($self) = @_;
    map { $_ => $self->$_ } qw(consumer_key consumer_secret token token_secret)
}

sub events
{
    my ($self) = @_;

    my %events = map { my $k = $_; $k => sub { $self->$k(@_) } }
                 qw(on_connect on_tweet on_friends
                    on_keepalive on_event on_error
                    on_eof on_delete);

    my %debug = map { my $k = $_; $k => sub { $self->on_debug($k, @_) } }
                qw(on_delete on_direct_message);

    (%events, %debug)
}


sub send
{
    my ($self, $line) = @_;
    eval { $self->commands->execute($line) };
    $self->ui->show_error($@) if $@;
}


sub reconnect
{
    my ($self) = @_;
    my $delay  = $self->stream->next_delay;
    $self->ui->append("Attempting reconnect in $delay seconds");
    $self->stream(Twt::Stream->new(parent => $self, delay => $delay));
}


sub on_tweet
{
    my ($self, $tweet) = @_;

    if (my $rt = $tweet->{retweeted_status})
    {
        my $rt_user = $rt->{user}{screen_name};
        $tweet->{text} = "RT \@$rt_user: $rt->{text}";
    }

    decode_entities($tweet->{text});
    $self->db->on_tweet($tweet);
    $self->ui->show_tweet(@{$tweet}{'user', 'text'});
}


sub on_delete
{
    my ($self, $tweet) = @_;
    $self->ui->append("Tweet $tweet->{id} deleted");

    my ($user, $text) = $self->db->tweet($tweet->{id});
    if (defined $user)
    {   $self->ui->show_tweet($user, "[deleted] $text") }
    else
    {   $self->ui->append("Never saw that tweet before") }
}


sub on_event
{
    my ($self, $event) = @_;
    $self->ui->show_event($event);
}


sub on_error
{
    my ($self, $error) = @_;
    $self->ui->show_error("$error in stream");
    $self->reconnect;
}

sub on_eof
{
    my ($self) = @_;
    $self->ui->show_error('unexpected end of file in stream');
    $self->reconnect;
}


sub on_connect
{
    my ($self) = @_;
    $self->stream->delay(0);
    $self->ui->append('Stream connected.');
    $self->on_tweet($_) for $self->rest->tweets_since_last_time;
}

sub on_friends {}
sub on_keepalive {}


sub on_debug
{
    my $self = shift;
    my $key  = shift;
    $self->ui->append($key, Dumper(@_));
}


sub run
{
    my ($self, $opt) = @_;
    $self->user;

    if ($opt->{nostream})
    {   $self->on_tweet($_) for $self->rest->tweets_since_last_time }
    else
    {   $self->stream }

    $self->ui->run;
    $self->quit;
}

sub quit
{
    my ($self) = @_;
    $self->clear_stream;
    $self->clear_rest;
}


no Moose;
1
