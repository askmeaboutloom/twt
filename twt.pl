#!/usr/bin/perl
use strict;
use warnings;
use Cwd                   qw(abs_path);
use Getopt::Long;
use File::Basename        qw(dirname);
use File::HomeDir         qw(my_data my_home);
use File::Spec::Functions qw(catfile catdir);
use Pod::Usage;
use Twt;


my %opt;
GetOptions \%opt, qw(
    nostream|S
      rcfile|r=s@
) or pod2usage(1);

if (@ARGV)
{
    my $args = join ', ', @ARGV;
    warn "Excessive arguments: $args\n";
    pod2usage(2);
}


sub load_config
{
    for my $rc (@_)
    {
        open my $fh, '<', $rc or next;
        my $code   = do { local $/; <$fh> };
        my $config = eval qq(use strict; use warnings;\n# line 1 "$rc"\n$code);

        die "Error parsing '$rc': $@.\n"       if $@;
        die "'$rc' didn't return a hashref.\n" if ref $config ne 'HASH';

        return $config;
    }
    die "No rc file found in any of these places:\n", map { "  * '$_'\n" } @_;
}

my @rcfiles = exists $opt{rcfile} ? @{$opt{rcfile}} : (
    catfile(my_home,                 '.twtrc'),
    catfile(catdir(my_data, '.twt'), '.twtrc'),
    catfile(dirname(abs_path $0),    '.twtrc'),
);

my $config = load_config(@rcfiles);


Twt->new($config)->run(\%opt);

__END__

=head1 NAME

twt - the Twitter client for 1980.

=head1 SYNOPSIS

    twt [--rcfile=RCFILE] [--nostream|-S]

=cut
